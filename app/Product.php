<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['title', 'local', 'description', 'user_id'];

    protected $dates = ['deleted_at'];

    function user() {
        return $this->belongsTo('App\User');
    }
}
