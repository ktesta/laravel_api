<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Hash;
use App\Http\Requests\AuthenticateRequest;

class AuthController extends Controller
{
    public function authenticate(AuthenticateRequest $request) {

      $credentials = $request->only('email', 'password');
      
      $validator = Validator::make($credentials, [
          'password' => 'required',
          'email' => 'required'
      ]);

      if($validator->fails()) {
          return response()->json([
              'message'   => 'Invalid credentials',
              'errors'        => $validator->errors()->all()
          ], 422);
      }

      $user = User::where('email', $credentials['email'])->first();

      if(!$user) {
        return response()->json([
          'error' => 'Invalid credentials'
        ], 401);
      }

      if (!Hash::check($credentials['password'], $user->password)) {
          return response()->json([
            'error' => 'Invalid credentials'
          ], 401);
      }

      $token = JWTAuth::fromUser($user);

      $objectToken = JWTAuth::setToken($token);
      $expiration = JWTAuth::decode($objectToken->getToken())->get('exp');

      return response()->json([
        'data' => [
          'access_token' => $token,
          'token_type' => 'bearer',
          'expires_in' => $expiration 
        ]
      ]);
    }

}
