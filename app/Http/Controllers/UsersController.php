<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;

class UsersController extends Controller
{
	public function __construct() 
	{
        $this->middleware('jwt.auth', ['except' => ['index', 'show', 'store']]);
    }

    public function index()
    {
        $users = User::all();
        return response()->json(['data' => $users]);
    }

    public function show($id)
    {
    	try {
	        $user = User::find($id);

	        if(!$user) {
	            return response()->json([
	                'message'   => 'Record not found',
	            ], 404);
	        }

	        return response()->json(['data' => $user]);

	    } catch(\Exception $e){
		    return response()->json([
	                'message'   => $e,
	            ], 404);
		}
    }

    public function store(Request $request)
    {
    	try {
    		$validator = Validator::make($request->all(), [
		        'name' => 'required|max:100',
		        'email' => 'required|email|unique:users',
		        'password' => 'required|max:100'
		    ]);

			if ($validator->fails() ) {
			    return response()->json([
			        'message'   => 'Validation Failed',
			        'errors'        => $validator->errors()
			    ], 422);
			}

	        $user = new User();
	        $user->fill($request->all());
			$password = $request->only('password')["password"];
	        $user->password = bcrypt($password);
	        $user->save();

	        return response()->json(['data' => $user], 201);

	    } catch(\Exception $e) {
		    return response()->json([
	                'message'   => $e,
	            ], 404);
		}
    }

    public function update(Request $request, $id)
    {
    	try {
	        $data = $request->all();
	        $user = User::find($id);

	        if (!$user) {
	            return response()->json([
	                'message'   => 'Record not found',
	            ], 404);
	        }

    		if (\Auth::user()->id != $user->id) {
            	return response()->json([
	                'message'   => 'You haven\'t permission to delete this entry',
	            ], 401);
	        }

	        //email doesn't change
	        if (array_key_exists('email', $data)) {
	            unset($data['email']);
	        }

	        $validator = Validator::make($data, [
	            'name' => 'max:100',
	        ]);

	        if ($validator->fails()) {
	            return response()->json([
	                'message'   => 'Validation Failed',
	                'errors'    => $validator->errors()->all()
	            ], 422);
	        }

	        $user->fill($data);
	        
	        if (array_key_exists('password', $data)) {
				$password = $request->only('password')["password"];
		        $user->password = bcrypt($password);
	    	}

	        $user->save();

	        return response()->json(['data' => $user]);

	    } catch(\Exception $e){
		    return response()->json([
	                'message'   => $e,
	            ], 404);
		}
    }

    public function destroy($id)
    {
    	try {

	        $user = User::find($id);

    		if (\Auth::user()->id != $user->id) {
            	return response()->json([
	                'message'   => 'You haven\'t permission to delete this entry',
	            ], 401);
	        }

	        if (!$user) {
	            return response()->json([
	                'message'   => 'Record not found',
	            ], 404);
	        }	        

	        return response()->json($user->delete(), 204);

        } catch(\Exception $e) {
		    return response()->json([
	                'message'   => $e,
	            ], 404);
		}
    }

}
