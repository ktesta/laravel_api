<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Product;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{	
	public function __construct() 
	{
        $this->middleware('jwt.auth', ['except' => ['index', 'show']]);
    }

    public function index()
    {
        $products = Product::with('user')->get();
        return response()->json(['data' => $products]);
    }

    public function show($id)
    {
    	try {
	        $product = Product::with('user')->find($id);

	        if (!$product) {
	            return response()->json([
	                'message'   => 'Record not found',
	            ], 404);
	        }

	        return response()->json(['data' => $product]);

	    } catch(\Exception $e){
		    return response()->json([
	                'message'   => $e,
	            ], 404);
		}
    }

    public function search(Request $request)
    {
    	try {
    		$params = $request->only('q');
    		$text = $params['q'];
	        $product = Product::with('user')->whereRaw("match(title, description, local) against (?)", [$text])->get();

	        if (!$product) {
	            return response()->json([
	                'message'   => 'Record not found',
	            ], 404);
	        }

	        return response()->json(['data' => $product]);

	    } catch(\Exception $e){
		    return response()->json([
	                'message'   => $e,
	            ], 404);
		}
    }

    public function store(Request $request)
    {
    	try {

    		$data = $request->all();

    		$validator = Validator::make($data, [
		        'title' => 'required',
		        'description' => 'required',
		        'local' => 'required',
		    ]);

			if ($validator->fails() ) {
			    return response()->json([
			        'message'   => 'Validation Failed',
			        'errors'        => $validator->errors()
			    ], 422);
			}

	        $product = new Product();
	        $product->fill($data);
	        $product->user_id = \Auth::user()->id;
	        $product->save();

	        return response()->json(['data' => $product], 201);

        } catch(\Exception $e){
		    return response()->json([
	                'message'   => $e,
				], 404);
		}
    }

    public function update(Request $request, $id)
    {
    	try {
    		$data = $request->all();

	        $product = Product::find($id);

	        if (!$product) {
	            return response()->json([
	                'message'   => 'Record not found',
	            	], 404);
	        }

	        if (\Auth::user()->id != $product->user_id) {
	            return response()->json([
	                'message'   => 'You haven\'t permission to change this entry',
	            ], 401);
	        }

	        $validator = Validator::make($data, [
		        'title' => 'max:100',
		        'description' => 'max:200',
		        'local' => 'max:100',
		    ]);

			if ($validator->fails() ) {
			    return response()->json([
			        'message'   => 'Validation Failed',
			        'errors'        => $validator->errors()
			    ], 422);
			}

	        $product->fill($data);
	        $product->user_id = \Auth::user()->id;
	        $product->save();

	        return response()->json(['data' => $product]);

	    } catch(\Exception $e){
		    return response()->json([
	                'message'   => $e,
	        	], 404);
		}
    }

    public function destroy($id)
    {	
    	try {
	        $product = Product::find($id);

	        if (!$product) {
	            return response()->json([
	                'message'   => 'Record not found',
	            ], 404);
	        }

	        if (\Auth::user()->id != $product->user_id) {
	            return response()->json([
	                'message'   => 'You haven\'t permission to delete this entry',
	            ], 401);
	        }

	        $product->delete();
	        
	    } catch(\Exception $e){
		    return response()->json([
	                'message'   => $e,
	            ], 404);
		}
    }
}
