# README #

Primeiro projeto com laravel

### USER ###
Lista de usuários
```
GET: http://localhost/api/public/api/users
```

Novo usuário
```
POST: http://localhost/api/public/api/users
{
  "name": "Kevin Testa",
  "email": "kevin@gmail.com",
  "password": "123123"
}
```

Login
```
POST: http://localhost/api/public/api/auth/login
{
  "email": "kevin@gmail.com",
  "password": "123123"
}
```

Atualizar usuário
```
PUT: http://localhost/api/public/api/users/1
{
  "token": "eyJ0eXAiOiJ",
  "name": "Kevin Cezar Testa"
}
```

Deletar usuário
```
DELETE: http://localhost/api/public/api/users/12
{
  "token": "eyJ0eXAi"
}
```

### PRODUCTS ###
Listar produtos
```
GET: http://localhost/api/public/api/products
```

Novo produto
```
POST: http://localhost/api/public/api/products
{
  "title": "Notebook",
  "local": "CPU D+ Informática",
  "description": "DELL INSPIRON",
  "token": "eyJ0eXA"
}
```

Atualizar produto
```
PUT: http://localhost/api/public/api/products/1
{
  "title": "Notebook",
  "local": "CPU D+ Informática",
  "description": "HP",
  "token": "eyJ0eXAiO"
}
```

Deletar produto
```
DELETE: http://localhost/api/public/api/api/products/16
{
  "token": "eyJ0eXAi"
}
```

Buscar produtos
```
GET: http://localhost/api/public/api/search?q=title&token=eyJ0eX
```