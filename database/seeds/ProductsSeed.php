<?php

use Illuminate\Database\Seeder;

class ProductsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Product::create([
            'title' => str_random(10),
            'description' => str_random(1000),
            'title' => str_random(10),
            'local' => 'São Paulo / SP',
            'user_id' => 1,
        ]);
    }
}
