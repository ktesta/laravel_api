<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function () {
  return response()->json(['message' => 'Products API', 'status' => 'Connected']);;
});

Route::resource('users', 'UsersController');
Route::resource('products', 'ProductsController');

Route::get('search', 'ProductsController@search');
Route::post('auth/login', 'AuthController@authenticate');


